package com.javacoo.swing.core.monitor;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;

import com.javacoo.swing.api.data.DataHandler;
import com.javacoo.swing.core.constant.Constant;
import com.javacoo.xkernel.spi.ExtensionLoader;
import com.teamdev.jxbrowser.chromium.DataReceivedParams;
import com.teamdev.jxbrowser.chromium.LoadURLParams;
import com.teamdev.jxbrowser.chromium.NetworkDelegate;

/**
 * 支付宝商家监控
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 10:37
 * @Version 1.0
 */
public class AliPayMonitorWindow extends BaseAliPayMonitorWindow {
    /**
     * 个人账户详细
     */
    private static final String ACCOUNT_DETAIL_URL = "https://mbillexprod.alipay.com/enterprise/fundAccountDetail.htm";


    public AliPayMonitorWindow(){
        super();
        dataHandler = ExtensionLoader.getExtensionLoader(DataHandler.class).getExtension(Constant.MONITOR_WINDOW_TYPE_DEFAULT);
    }
    @Override
    protected void onFinishLoaded(){}
    /**
     * 获取加载URL参数
     * <li></li>
     *
     * @author duanyong@jccfc.com
     * @date 2020/9/15 17:09
     * @return: com.teamdev.jxbrowser.chromium.LoadURLParams
     */
    @Override
    protected LoadURLParams getLoadURLParams() {
        return new LoadURLParams(ACCOUNT_DETAIL_URL);
    }
    @Override
    protected NetworkDelegate getNetworkDelegate() {
        return new MyNetworkDelegate(){
            @Override
            public void onDataReceived(DataReceivedParams params){
                //精准处理接口响应结果
                if(params.getURL().contains(dataHandler.getMonitorUrl())){
                    try {
                        dataHandler.handle(new String(params.getData(), StringUtils.isNotBlank(params.getCharset()) ? params.getCharset() : "gbk"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }
}
