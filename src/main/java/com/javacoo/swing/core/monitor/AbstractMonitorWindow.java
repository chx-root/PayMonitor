package com.javacoo.swing.core.monitor;

import com.google.common.eventbus.Subscribe;
import com.javacoo.swing.api.data.DataHandler;
import com.javacoo.swing.api.monitor.MonitorWindow;
import com.javacoo.swing.core.event.DataEvent;
import com.javacoo.swing.core.event.DataType;
import com.javacoo.swing.core.event.MonitorActionEvent;
import com.javacoo.swing.core.event.MonitorEventBus;
import com.javacoo.swing.core.utils.OtherUtil;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.Callback;
import com.teamdev.jxbrowser.chromium.LoadURLParams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 监控窗口抽象实现类型
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 10:43
 * @Version 1.0
 */
public abstract class AbstractMonitorWindow extends JFrame implements MonitorWindow {
    protected static Logger log = LoggerFactory.getLogger(AbstractMonitorWindow.class);
    /**
     * 窗体默认宽度
     */
    protected static final int FRAME_WIDTH = 520;
    /**
     * 窗体默认高度
     */
    protected static final int FRAME_HEIGHT = 500;
    /**
     * 浏览器对象
     */
    protected Browser browser;
    /**数据处理服务*/
    protected DataHandler dataHandler;

    private int min = 10;

    private int max = 20;
    /**
     * 是否初始化
     */
    protected boolean init;
    /**
     * 定时任务
     */
    protected ScheduledExecutorService service = null;

    public AbstractMonitorWindow(){
        setVisible(false);
        MonitorEventBus.getInstance().register(this);
    }

    @Override
    public final void start() {
        initWindow();
    }

    protected abstract void initWindow();

    protected abstract void doHandleActionEvent(MonitorActionEvent actionEvent);

    @Subscribe
    private void handleActionEvent(MonitorActionEvent actionEvent){
        doHandleActionEvent(actionEvent);
    }
    /**
     * 启动定时任务
     * <p>说明:</p>
     * <li></li>
     * @Author DuanYong
     * @Since 2019/9/1 11:29
     * @Version 1.0
     * @Params browser
     */
    protected void startSchedule(){
        service = Executors.newSingleThreadScheduledExecutor();
        int delay = OtherUtil.getRandom(min,max);
        log.info("启动定时任务,每隔:{}秒执行一次",delay);
        MonitorEventBus.getInstance().post(new DataEvent(DataType.LOG_DATA,"启动定时任务,每隔:"+delay+"秒执行一次"));
        service.scheduleWithFixedDelay(() -> browser.reload(), 20,delay, TimeUnit.SECONDS);
    }

    protected void setMin(Object min) {
        if(null != min){
            try{
                this.min = Integer.valueOf(min.toString());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    protected void setMax(Object max) {
        if(null != max){
            try{
                this.max = Integer.valueOf(max.toString());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    /**
     * 加载并等待URL完成
     * <li></li>
     * @author duanyong@jccfc.com
     * @date 2020/9/16 13:56
     * @param browser:浏览器对象
     * @param loadURLParams:地址参数
     * @return: void
     */
    protected void loadURLAndWaitReady(Browser browser, final LoadURLParams loadURLParams) {
        Browser.invokeAndWaitFinishLoadingMainFrame(browser, value -> value.loadURL(loadURLParams));
    }

}
