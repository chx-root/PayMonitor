package com.javacoo.swing.core.event;

import com.alibaba.fastjson.JSONObject;
import com.javacoo.swing.core.constant.Constant;

/**
 * 数据事件
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/8/31 20:07
 * @Version 1.0
 */
public class DataEvent {
    /**数据类型*/
    private DataType dataType;
    /**数据*/
    private JSONObject data;

    public DataEvent(DataType dataType,JSONObject data) {
        this.dataType = dataType;
        this.data = data;
    }
    public DataEvent(DataType dataType,Object data) {
        this.dataType = dataType;
        this.data = new JSONObject();
        this.data.put(Constant.DATA_KEY,data);
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
