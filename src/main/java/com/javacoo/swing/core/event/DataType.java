package com.javacoo.swing.core.event;

/**
 * 数据类型
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 14:11
 * @Version 1.0
 */
public enum DataType {
    LIST_DATA,LOG_DATA,UID_DATA,TIMER_DATA;
}
