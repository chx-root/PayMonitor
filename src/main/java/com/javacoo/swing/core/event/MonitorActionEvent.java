package com.javacoo.swing.core.event;

import com.alibaba.fastjson.JSONObject;

/**
 * 动作事件
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 11:09
 * @Version 1.0
 */
public class MonitorActionEvent {
    /**动作类型*/
    private ActionType actionType;
    /**数据*/
    private JSONObject data;

    public MonitorActionEvent(ActionType actionType) {
        this.actionType = actionType;
    }
    public MonitorActionEvent(ActionType actionType,JSONObject data) {
        this.actionType = actionType;
        this.data = data;
    }
    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
