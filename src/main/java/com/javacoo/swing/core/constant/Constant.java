/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package com.javacoo.swing.core.constant;

/**
 *  常量定义
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2019/8/29 11:50
 * @version 1.0
 */
public interface Constant {
	/**
	 * 窗体默认宽度
	 */
	int FRAME_DEFAULT_WIDTH = 1280;
	/**
	 * 窗体默认高度
	 */
	int FRAME_DEFAULT_HEIGHT = 768;

	/**
	 * 帮助窗体默认宽度
	 */
	int HELP_FRAME_DEFAULT_WIDTH = 600;
	/**
	 * 帮助窗体默认高度
	 */
	int HELP_FRAME_DEFAULT_HEIGHT = 415;

	String SYSTEM_ROOT_PATH = System.getProperties().getProperty("user.dir");

	String SYSTEM_SEPARATOR = System.getProperties().getProperty("file.separator");
	/** 系统行分隔符 */
	String SYSTEM_LINE_SEPARATOR = System.getProperty("line.separator");


	String CONFIG_PATH_KEY = "path";
	String DATA_KEY = "data";


	String PARAM_KEY = "key";
	String PARAM_VALUE = "value";
	String PARAM_COMMENT = "comment";
	String PARAM_APPEND = "append";
	/** 需要监控的请求URL */
	String MONITOR_URL_KEY="fundAccountDetail.json";
	/** 远程调用响应码KEY */
	String REMOTE_RETURN_KEY = "retCode";
	/** 远程调用响应码:成功 */
	String REMOTE_RETURN_VALUE_SUCCESS = "SUCCESS";

	String TIMER_MIN_KEY= "min";

	String TIMER_MAX_KEY = "max";
	/**默认类型*/
	String MONITOR_WINDOW_TYPE_DEFAULT = "default";
	/**我的账单*/
	String MONITOR_WINDOW_TYPE_MY_BILL = "myBillRecord";
	/**商户密钥*/
	String SECRET_KEY_CONSTANT="secretkey";



}
