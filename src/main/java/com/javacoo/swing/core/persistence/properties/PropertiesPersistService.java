package com.javacoo.swing.core.persistence.properties;

import com.alibaba.fastjson.JSONObject;
import com.javacoo.swing.api.persistence.PersistService;
import com.javacoo.swing.core.constant.Constant;
import com.javacoo.swing.core.utils.PropertiesHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.Asserts;

import java.util.*;

/**
 * 基于properties实现持久化服务
 * <p>说明:</p>
 * <li></li>
 *
 * @author DuanYong
 * @version 1.0
 * @since 2019/8/30 10:21
 */
@Slf4j
public class PropertiesPersistService implements PersistService<JSONObject> {
    /**配置参数文件路径*/
    private static String DATA_PATH = Constant.SYSTEM_ROOT_PATH+"/data/data.properties";
    //private static String DATA_PATH = "/resources/data.properties";
    private static String CLASS_LODER_PATH = PropertiesPersistService.class.getClassLoader().getResource("").getPath();

    @Override
    public int insert(JSONObject parameterObject) {
        Asserts.notNull(parameterObject,"新增数据");
        Asserts.notNull(parameterObject.get(Constant.PARAM_KEY),"键"+Constant.PARAM_KEY);
        Asserts.notNull(parameterObject.get(Constant.PARAM_VALUE),"值"+Constant.PARAM_VALUE);
        Asserts.notNull(parameterObject.get(Constant.PARAM_COMMENT),"注释"+Constant.PARAM_COMMENT);
        boolean append = true;
        if(null != parameterObject.get(Constant.PARAM_APPEND) && "false".equalsIgnoreCase(parameterObject.get(Constant.PARAM_APPEND).toString())){
            append = false;
        }
        String path = DATA_PATH;
        log.info("写入配置文件:{}",DATA_PATH);
        PropertiesHelper.writeProperties(path,parameterObject.get(Constant.PARAM_KEY).toString(),parameterObject.get(Constant.PARAM_VALUE).toString(),parameterObject.get(Constant.PARAM_COMMENT).toString(),append);
        return 1;
    }

    @Override
    public int update(JSONObject parameterObject) {
        return 0;
    }

    @Override
    public int delete(JSONObject parameterObject) {
        return 0;
    }

    @Override
    public JSONObject get(JSONObject parameterObject) {
        return null;
    }

    @Override
    public Optional<List<JSONObject>> getList(JSONObject parameterObject) {
        log.info("开始加载数据配置文件:{}",DATA_PATH);
        Properties properties = PropertiesHelper.loadProperties(DATA_PATH);
        Enumeration<?> propNames = properties.propertyNames();
        String propKey = null;
        String propValue = null;
        List<JSONObject> result = new ArrayList<>();
        JSONObject jsonObject = null;
        while (propNames.hasMoreElements()) {
            propKey = (String) propNames.nextElement();
            propValue = properties.getProperty(propKey);
            log.info("参数KEY：{},值为：{}",propKey,propValue);
            if(StringUtils.isNotBlank(propKey) && StringUtils.isNotBlank(propValue)){
                jsonObject = new JSONObject();
                jsonObject.put(propKey,propValue);
                result.add(jsonObject);
            }
        }
        log.info("数据文件加载完成");
        return Optional.ofNullable(result);
    }
}
