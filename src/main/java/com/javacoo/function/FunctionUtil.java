package com.javacoo.function;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * 函数工具类
 * <p>说明:</p>
 * <li></li>
 *
 * @author duanyong@jccfc.com
 * @date 2020/8/1 21:52
 */
@Slf4j
public class FunctionUtil {


    public static Function<Integer,Integer> compose(Function<Integer,Integer> f1,Function<Integer,Integer> f2){
        return new Function<Integer,Integer>(){
            @Override
            public Integer apply(Integer integer) {
                return f1.apply(f2.apply(integer));
            }
        };
    }
    public static Function<Integer,Integer> compose2(Function<Integer,Integer> f1,Function<Integer,Integer> f2){
        return integer->f1.apply(f2.apply(integer));
    }

    public static <T,U,V> Function<Function<T,U>,Function<Function<V,T>,Function<V,U>>> compose3() {
        return f->g->x->f.apply(g.apply(x));
    }

    public static Function<Integer,Function<Integer,Integer>> add = x->y->x+y;




    public static void main(String[] args) {
        BigDecimal deductionAmt = new BigDecimal("300");
        BigDecimal fineAmt = new BigDecimal("100");
        BigDecimal interestAmt = new BigDecimal("300");
        BigDecimal principalAmt = new BigDecimal("1300");

    }

}
